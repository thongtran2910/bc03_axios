function ValidatorUser() {
  this.checkEmpty = function (idTarget, idError, messageError) {
    var valueTarget = document.getElementById(idTarget).value.trim();
    if (!valueTarget) {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
  this.checkAccountValid = function (newUser, userList) {
    var index = userList.findIndex(function (item) {
      return item.id == newUser.id;
    });
    if (index == -1) {
      document.getElementById("spanTaiKhoan").innerText = "";
      return true;
    } else {
      document.getElementById("spanTaiKhoan").innerText =
        "Tài khoản không được trùng";
      return false;
    }
  };
  this.checkEmailValid = function (idTarget, idError) {
    var pattern =
      /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
    var valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Email không hợp lệ";
      return false;
    }
  };
  this.checkNameValid = function (idTarget, idError) {
    var pattern = /^(?!\s+$)[a-zA-Z,'. -]+$/;
    var valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Họ tên không được chứa số và ký tự đặc biệt";
      return false;
    }
  };
  this.checkPasswordValid = function (idTarget, idError) {
    var pattern =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,8}$/;
    var valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Mật khẩu phải có ít nhất 1 ký tự hoa, 1 ký tự đặc biệt, 1 chữ số, dài từ 6-8 ký tự";
      return false;
    }
  };
  this.checkUserValid = function (idTarget, idError) {
    var pattern = /^(?!blank$).*/;
    var valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Vui lòng chọn người dùng";
      return false;
    }
  };
  this.checkLanguage = function (idTarget, idError) {
    var pattern = /^(?!blank$).*/;
    var valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Vui lòng chọn ngôn ngữ";
      return false;
    }
  };
  this.checkDesValid = function (idTarget, idError) {
    var pattern = /^.{1,60}$/;
    var valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Mô tả không được vượt quá 60 ký tự";
      return false;
    }
  };
}
