var validatorUser = new ValidatorUser();
const validator = function (userList) {
  var newUser = getValueFromForm();
  var userList = [];
  var isValid = true;
  var isValidAccount =
    validatorUser.checkEmpty(
      "TaiKhoan",
      "spanTaiKhoan",
      "Tài khoản không được bỏ trống"
    ) && validatorUser.checkAccountValid(newUser, userList);
  var isValidName =
    validatorUser.checkEmpty(
      "HoTen",
      "spanHoTen",
      "Họ tên không được bỏ trống"
    ) && validatorUser.checkNameValid("HoTen", "spanHoTen");
  var isValidPassword =
    validatorUser.checkEmpty(
      "MatKhau",
      "spanMatKhau",
      "Mật khẩu không được bỏ trống"
    ) && validatorUser.checkPasswordValid("MatKhau", "spanMatKhau");
  var isValidEmail =
    validatorUser.checkEmpty(
      "Email",
      "spanEmail",
      "Email không được bỏ trống"
    ) && validatorUser.checkEmailValid("Email", "spanEmail");
  var isValidImg = validatorUser.checkEmpty(
    "HinhAnh",
    "spanHinhAnh",
    "Hình ảnh không được bỏ trống"
  );
  var isValidUser = validatorUser.checkUserValid("loaiNguoiDung", "spanLoaiNd");
  var isValidLanguage = validatorUser.checkLanguage(
    "loaiNgonNgu",
    "spanNgonNgu"
  );
  var isValidDes =
    validatorUser.checkEmpty("MoTa", "spanMoTa", "Mô tả không được bỏ trống") &&
    validatorUser.checkDesValid("MoTa", "spanMoTa");
  return (isValid =
    isValidAccount &&
    isValidName &&
    isValidEmail &&
    isValidImg &&
    isValidUser &&
    isValidLanguage &&
    isValidPassword &&
    isValidDes);
};
const turnOnLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
const turnOffLoading = function () {
  document.getElementById("loading").style.display = "none";
};
const renderUserListServ = function () {
  turnOnLoading();
  userServ
    .getList()
    .then(function (res) {
      turnOffLoading();
      renderUserList(res.data);
    })
    .catch(function (err) {
      turnOffLoading;
    });
};
renderUserListServ();

const getValueFromForm = function () {
  var taiKhoanND = document.getElementById("TaiKhoan").value;
  var hoTenND = document.getElementById("HoTen").value;
  var matKhauND = document.getElementById("MatKhau").value;
  var emailND = document.getElementById("Email").value;
  var hinhAnhND = document.getElementById("HinhAnh").value;
  var loaiND = document.getElementById("loaiNguoiDung").value;
  var ngonNgu = document.getElementById("loaiNgonNgu").value;
  var moTa = document.getElementById("MoTa").value;

  return new User(
    taiKhoanND,
    hoTenND,
    matKhauND,
    emailND,
    hinhAnhND,
    loaiND,
    ngonNgu,
    moTa
  );
};

const renderUserList = function (userList) {
  var contentHTML = "";
  userList.forEach(function (item) {
    var contentTrTag = `<tr>
        <td>${item.id}</td>
        <td>${item.account}</td>
        <td>${item.password}</td>
        <td>${item.name}</td>
        <td>${item.email}</td>
        <td>${item.language}</td>
        <td>${item.type}</td>
        <td>
        <button onclick="editUserInfo(${item.id})" class="btn btn-info" data-toggle="modal"
        data-target="#myModal">Sửa</button>
        <button onclick="deleteUser(${item.id})" class="btn btn-danger">Xoá</button>
        </td>
        </tr>`;
    contentHTML += contentTrTag;
  });
  document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
};
const addNewUser = function () {
  var newUser = getValueFromForm();
  turnOnLoading();
  if (validator()) {
    axios({
      url: `${BASE_URL}/nguoi-dung`,
      method: "POST",
      data: newUser,
    })
      .then(function (res) {
        turnOffLoading();
        renderUserListServ(res.data);
        $("#myModal").modal("hide");
      })
      .catch(function (err) {
        turnOffLoading();
      });
    turnOffLoading();
  }
};

const renderUserInfoToForm = function (data) {
  document.getElementById("TaiKhoan").value = data.account;
  document.getElementById("HoTen").value = data.name;
  document.getElementById("MatKhau").value = data.password;
  document.getElementById("Email").value = data.email;
  document.getElementById("HinhAnh").value = data.img;
  document.getElementById("loaiNguoiDung").value = data.type;
  document.getElementById("loaiNgonNgu").value = data.language;
  document.getElementById("MoTa").value = data.description;
};

const editUserInfo = function (id) {
  turnOnLoading();
  document.getElementById("btn-add").style.display = "none";
  document.getElementById("btn-update").style.display = "block";
  document.getElementById("id-user").style.display = "none";
  axios({
    url: `${BASE_URL}/nguoi-dung/${id}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      renderUserInfoToForm(res.data);
      document.querySelector("#id-user span").innerHTML = res.data.id;
    })
    .catch(function (err) {
      turnOffLoading();
    });
};

const updateUserInfo = function () {
  turnOnLoading();
  var updatedUser = getValueFromForm();
  var idUser = document.querySelector("#id-user span").innerHTML * 1;
  if (validator()) {
    axios({
      url: `${BASE_URL}/nguoi-dung/${idUser}`,
      method: "PUT",
      data: updatedUser,
    })
      .then(function (res) {
        $("#myModal").modal("hide");
        turnOffLoading();
        renderUserListServ(res.data);
      })
      .catch(function (err) {
        turnOffLoading();
      });
  }
};

const deleteUser = function (id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/nguoi-dung/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      renderUserListServ(res);
    })
    .catch(function (err) {
      turnOffLoading();
    });
};

document
  .getElementById("btnThemNguoiDung")
  .addEventListener("click", function () {
    document.getElementById("form-user").reset();
    document.getElementById("id-user").style.display = "none";
    document.getElementById("btn-add").style.display = "block";
    document.getElementById("btn-update").style.display = "none";
  });
