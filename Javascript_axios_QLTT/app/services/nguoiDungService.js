const BASE_URL = "https://6271e185c455a64564b8efa7.mockapi.io";

const userServ = {
  getList: function () {
    return axios({
      url: `${BASE_URL}/nguoi-dung`,
      method: "GET",
    });
  },
};
